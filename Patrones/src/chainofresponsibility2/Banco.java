package chainofresponsibility2;

public class Banco implements IAprobador {

	private IAprobador next;

	@Override
	public void setNext(IAprobador iaprobador) {
		next = iaprobador;

	}

	@Override
	public IAprobador getNext() {
		return next;
	}

	@Override
	public void solicitarPrestamos(int monto) {
		ChainDirector director = new ChainDirector();
		Builder builder = new ChainBuilder();
		director.construct(builder);
		IAprobador aprobador = ((ChainBuilder)builder).getResult();
		setNext(aprobador);
		next.solicitarPrestamos(monto);
	}

}
