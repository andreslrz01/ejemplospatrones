package chainofresponsibility2;

public class LiderTeamEjecutivo implements IAprobador {

	private IAprobador next;
	
	@Override
	public void setNext(IAprobador iaprobador) {
		next = iaprobador;

	}

	@Override
	public IAprobador getNext() {
		return next;
	}

	@Override
	public void solicitarPrestamos(int monto) {
		if(monto > 10000 && monto <= 50000) {
			System.out.println("Lo manejo yo, el lider");
			//next.solicitarPrestamos(monto+50000);
		} else {
			next.solicitarPrestamos(monto);
		}
	}

}
