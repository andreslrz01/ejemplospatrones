package chainofresponsibility2;

public interface Builder {
	public abstract void buildPart();
}
