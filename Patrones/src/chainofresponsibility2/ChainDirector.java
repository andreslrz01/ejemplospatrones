package chainofresponsibility2;

public class ChainDirector {
	public void construct(Builder builder) {
		builder.buildPart();
	}
}
