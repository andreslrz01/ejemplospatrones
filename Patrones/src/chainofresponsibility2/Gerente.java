package chainofresponsibility2;

public class Gerente implements IAprobador {

	private IAprobador next;

	@Override
	public void setNext(IAprobador iaprobador) {
		next = iaprobador;

	}

	@Override
	public IAprobador getNext() {
		return next;
	}

	@Override
	public void solicitarPrestamos(int monto) {
		if (monto > 50000 && monto <= 100000) {
			System.out.println("Lo manejo yo, el gerente");
		} else {
			next.solicitarPrestamos(monto);
		}
	}

}
