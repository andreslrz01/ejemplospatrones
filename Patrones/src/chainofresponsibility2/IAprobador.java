package chainofresponsibility2;

public interface IAprobador {
	public void setNext(IAprobador iaprobador);
	public IAprobador getNext();
	public void solicitarPrestamos(int monto);
}
