package chainofresponsibility2;

public class ChainBuilder implements Builder {

	private IAprobador iaprobador;
	
	@Override
	public void buildPart() {
		buildDirector();
		buildGerente();
		buildLiderEjecutivoTeam();
		buildEjecutivoDeCuentas();
	}
	
	public void buildDirector() {
		IAprobador director = new Director();
		setIAprobador(director);
	}
	
	public void buildGerente() {
		IAprobador gerente = new Gerente();
		gerente.setNext(iaprobador);
		setIAprobador(gerente);
	}
	
	public void buildLiderEjecutivoTeam() {
		IAprobador liderEjecutivo = new LiderTeamEjecutivo();
		liderEjecutivo.setNext(iaprobador);
		setIAprobador(liderEjecutivo);
	}
	
	public void buildEjecutivoDeCuentas() {
		IAprobador ejecutivoCuentas = new EjecutioDeCuenta();
		ejecutivoCuentas.setNext(iaprobador);
		setIAprobador(ejecutivoCuentas);
	}
	
	private void setIAprobador(IAprobador producto) {
		this.iaprobador = producto;
	}

	public IAprobador getResult() {
		return iaprobador;
	}
}
