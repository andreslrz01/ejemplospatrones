package bridge;

public interface WindowImpl {
	public void devDrawText();
	public void devDrawLine();
}
