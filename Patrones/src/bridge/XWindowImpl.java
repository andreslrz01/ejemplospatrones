package bridge;

public class XWindowImpl implements WindowImpl {

	@Override
	public void devDrawText() {
		System.out.println("Mostrando texto del borde para XWindow");

	}

	@Override
	public void devDrawLine() {
		System.out.println("Mostrando borde para XWindow");

	}

}
