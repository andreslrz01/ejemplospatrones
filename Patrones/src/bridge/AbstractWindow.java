package bridge;

public abstract class AbstractWindow {
	
	private WindowImpl window;
	
	public void drawText() {
		window.devDrawText();
	}
	
	public void drawRect() {
		window.devDrawLine();
	}
	
	public void setWindowImpl(WindowImpl window) {
		this.window = window;
	}
}
