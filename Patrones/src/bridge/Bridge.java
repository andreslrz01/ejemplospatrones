package bridge;

public class Bridge {

	public static void main(String[] args) {
		IconWindow window = new IconWindow();
		WindowImpl impl1 = new XWindowImpl();
		WindowImpl impl2 = new PMWindowImpl();
		window.setWindowImpl(impl1);
		window.drawBorder();
		window.setWindowImpl(impl2);
		window.drawBorder();
		TransientWindow window2 = new TransientWindow();
		window2.setWindowImpl(impl1);
		window2.drawCloseBox();
		window2.setWindowImpl(impl2);
		window2.drawCloseBox();
	}

}
