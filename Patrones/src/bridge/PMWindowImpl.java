package bridge;

public class PMWindowImpl implements WindowImpl {

	@Override
	public void devDrawText() {
		System.out.println("Mostrando texto del borde para PMWindow");
	}

	@Override
	public void devDrawLine() {
		System.out.println("Mostrando borde para PMWindow");
	}

}
