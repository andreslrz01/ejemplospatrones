package bridge;

public class IconWindow extends AbstractWindow {

	public void drawBorder() {
		drawRect();
		drawText();
	}
}
