package chainofresponsibility;

public class Dialog extends Widget {

	public Dialog(HelpHandler successor) {
		super(successor);
	}

	@Override
	public void handleHelp() {
		if(hasHelp()) {
			System.out.println("Ayuda del dialog!!");
		} else {
			HelpHandler succesor = getSuccessor();
			succesor.handleHelp();
		}

	}

}
