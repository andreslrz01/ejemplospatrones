package chainofresponsibility;

public abstract class Widget extends HelpHandler {

	

	public Widget(HelpHandler successor) {
		super();		
	}

	@Override
	public abstract void handleHelp();

}
