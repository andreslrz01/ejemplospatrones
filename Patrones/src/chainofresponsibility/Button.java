package chainofresponsibility;

public class Button extends Widget {

	public Button(HelpHandler successor) {
		super(successor);
	}

	@Override
	public void handleHelp() {
		if(hasHelp()) {
			System.out.println("Ayuda de boton!!");
		} else {
			HelpHandler succesor = getSuccessor();
			succesor.handleHelp();
		}
	}

}
