package chainofresponsibility;

public abstract class HelpHandler {

	public static final int NO_HELP_TOPIC = -1;
	private int topic;
	private HelpHandler successor;

	public HelpHandler() {
		super();
		topic = NO_HELP_TOPIC;
	}

	public HelpHandler(int topic, HelpHandler successor) {
		super();
		this.topic = topic;
		this.successor = successor;
	}

	public abstract void handleHelp();

	public boolean hasHelp() {
		return HelpHandler.NO_HELP_TOPIC != topic;
	}

	public void setTopic(int topic) {
		this.topic = topic;
	}

	public int getTopic() {
		return topic;
	}

	public HelpHandler getSuccessor() {
		return successor;
	}

	public void setSuccessor(HelpHandler successor) {
		this.successor = successor;
	}

}
